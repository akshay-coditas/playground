import { Component } from '@angular/core';
import { appManager } from 'orxe-shell';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-wrap';

  constructor() {
    const routes = [
      // declare route configuration
      {
        path: 'hotel',
        tagName: 'orxe-hotel-search',
        preload: {
          dependencies: []
        }
      },
      {
        path: 'details',
        tagName: 'orxe-hotel-details',
        preload: {
          dependencies: ['orxe-hotel-search']
        }
      }
    ];
    // appManager.init({ routes }); // initialize app with route configuration

    // appManager.router.navigate('details');
  }
}
